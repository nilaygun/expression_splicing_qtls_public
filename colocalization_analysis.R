options(stringsAsFactors=FALSE)
library(data.table)
# Preferences
k = commandArgs(trailingOnly=TRUE)[1]
gwas = commandArgs(trailingOnly=TRUE)[2]
type = commandArgs(trailingOnly=TRUE)[3]

#link1 = "/pine/scr/n/i/nil1/diseasecoloc/"
if (type == "progenitor"){
    pvalue= 2.680431e-05
    L = "P"
    link1 = "/pine/scr/n/i/nil1/diseasecoloc/eqtl/progenitor/"
}else if (type == "neuron"){
    pvalue= 1.848316e-06
    L = "N"
    link1 = "/pine/scr/n/i/nil1/diseasecoloc/eqtl/neuron/"
}else{
    pvalue= 0.0008168189
    L = "F"
    link1 = "/pine/scr/n/i/nil1/diseasecoloc/eqtl/bulk/"

}

# plink function to generate .raw file followed by printing allele dosage

makeRaw = function(snp,chr,out) {
  if (type == "progenitor"){
      system(paste0("plink --bfile /proj/steinlab/projects/R00/eQTLanalysis/genofiles/allele_order_corrected_genofiles/chr",k,"_P.uniq --out ",out,
         " --recode AD --snps ",snp))
  }else if (type == "neuron"){
       system(paste0("plink --bfile /proj/steinlab/projects/R00/eQTLanalysis/genofiles/allele_order_corrected_genofiles/chr",k,"_N.uniq --out ",out,
         " --recode AD --snps ",snp))
  }else{
      system(paste0("plink --bfile /proj/steinlab/projects/R00/eQTLanalysis/genofiles/allele_order_corrected_genofiles/bulk/chr",k,"_F.uniq --out ",out,
         " --recode AD --snps ",snp))
  }
  raw = read.table(paste0(out,".raw"),skip=1)
  return(raw$V7)
}

# EMMAX function followed by printing association with the snp of interest


emmax = function(snp,geno,pheno,kinship,cov,pval,out) {
  gene1 = sapply(as.character(out),function(x) unlist(strsplit(x,"-",fixed="TRUE"))[3])
  system(paste0("emmax -v -d 10 -t ",geno," -p ",pheno," -k ",kinship," -c ",cov," -o ",out),wait=TRUE)
  if (file.exists(paste0(out[1],".ps")) == TRUE){
     ps = read.table(paste0(out[1],".ps"))
     sig.ps = ps[which(ps$V1 == snp),]
     return(sig.ps)

 }
}
eqtl = fread(paste0("/proj/steinlab/projects/R00/eQTLanalysis/",type,".cond.independent.txt"))


#eqtl$BP = as.numeric(sapply(as.character(eqtl$SNP_B), function(x) unlist(strsplit(x,":",fixed="TRUE"))[2]))
eqtl$CHR = sapply(as.character(eqtl$snp), function(x) unlist(strsplit(x,":",fixed="TRUE"))[1])
gw = fread(paste0("/proj/steinlab/projects/R00/eQTLanalysis/Diseasecoloc/scripts/",gwas,"_index_r05.txt"))
ld.R00 = fread(paste0("/proj/steinlab/projects/R00/eQTLanalysis/Clumped_data/LD_buddies/R00.",type,".conditional.snps.csv"))
ld.EUR = fread(paste0("/proj/steinlab/projects/R00/eQTLanalysis/Clumped_data/LD_buddies/EUR.",type,".conditional.snps.csv"))
# Subset for chromosome
gw = gw[which(gw$CHR == k),]
eqtl = eqtl[which(eqtl$CHR == paste0("chr",k)),]
ld.R00 = ld.R00[which(ld.R00$chr == paste0("chr",k)),]
ld.R00$BP_B = as.numeric(sapply(as.character(ld.R00$SNP_B), function(x) unlist(strsplit(x,":",fixed="TRUE"))[2]))
ld.EUR = ld.EUR[which(ld.EUR$chr == paste0("chr",k)),]
bim = fread(paste0("/proj/steinlab/projects/1000genomes/phase3EURhg38/ALL.chr",k,"_GRCh38.genotypes.20170504.EUR.bim"))
ld.EUR$BP_B = bim$V4[match(ld.EUR$SNP_B,bim$V2)]

r00 = data.frame()
eur = data.frame()
for (i in 1:nrow(eqtl)){
     snp.R00 = ld.R00[which(ld.R00$snp == eqtl$snp[i] & ld.R00$r2 > 0.8),]
     snp.R00$POP = "Study"
     snp.EUR = ld.EUR[which(ld.EUR$snp == eqtl$snp[i] & ld.EUR$r2 > 0.8),]
     snp.EUR$POP = "European"
     gw.R00 = intersect(snp.R00$BP_B,gw$BP)
     gw.EUR = intersect(snp.EUR$BP_B,gw$BP)
     if (length(gw.R00) >= 1){
        r00 = rbind(r00,data.frame(snp.R00[which(snp.R00$BP_B %in% gw.R00),]))
     }
     if (length(gw.EUR) >= 1){
        eur = rbind(eur,data.frame(snp.EUR[which(snp.EUR$BP_B %in% gw.EUR),]))
     }

}


if (type == "progenitor" | type == "neuron"){
       bim = fread(paste0("/proj/steinlab/projects/R00/eQTLanalysis/genofiles/allele_order_corrected_genofiles/chr",k,"_",L,".uniq.bim"))
   }else{
       bim = fread(paste0("/proj/steinlab/projects/R00/eQTLanalysis/genofiles/allele_order_corrected_genofiles/bulk/chr",k,"_F.uniq.bim"))
    }
data = rbind(r00,eur)
if (nrow(data) >= 1){
# Perform conditional analysis via controlling for GWAS index snp on eQTL index snp

# Remove the variants with ambiguity
if(length(grep("<",data$SNP_B)) >= 1) {
  data = data[-grep("<",data$SNP_B),]
}

# Convert rsID to variant ID
if (length(grep("rs",data$SNP_B)) >= 1){
    rs = data[grep("rs",data$SNP_B),]
   if (type == "progenitor" | type == "neuron"){
       bim = fread(paste0("/proj/steinlab/projects/R00/eQTLanalysis/genofiles/allele_order_corrected_genofiles/chr",k,"_",L,".uniq.bim"))
   }else{
       bim = fread(paste0("/proj/steinlab/projects/R00/eQTLanalysis/genofiles/allele_order_corrected_genofiles/bulk/chr",k,"_F.uniq.bim"))
    }
    exc.snp = c()
    for (i in 1:nrow(rs)) {
         if (length(which(bim$V4 %in% rs$BP_B[i])) >= 1){
             snp = bim$V2[which(bim$V4 %in% rs$BP_B[i])]
             index = which(data$BP_B %in% rs$BP_B[i])
             data$SNP_B[index] = snp
         }else{
             exc.snp = c(exc.snp,rs$SNP_B[i])
         }
    }
   if(length(exc.snp) >= 1){
     data = data[-which(data$SNP_B %in% exc.snp),]
     write.table(data[which(data$SNP_B %in% exc.snp),],file=paste0("/pine/scr/n/i/nil1/",gwas,"_Coincedence_excluded_snps__",type,"_chr",k,".txt"),quote=FALSE,col.names=TRUE,row.names=TRUE)
   }
}

write.table(data,file=paste0(gwas,"_Coincedence_",type,"_chr",k,".txt"),quote=FALSE,col.names=TRUE,row.names=FALSE)
### First Conditional Analysis #####
res = data.frame()
data = data[which(data$SNP_B %in% intersect(data$SNP_B,bim$V2)),]

for (j in 1:nrow(data)) {
  if (type == "progenitor"){
      L = "P"
      kinship = paste0("/proj/steinlab/projects/R00/eQTLanalysis/genofiles/snpsprowo",k,".hBN.kinf")
      raw.cov = read.table("/proj/steinlab/projects/R00/eQTLanalysis/genofiles/covariates/new.pc.pro10.txt")
  }else if (type == "neuron"){
      L = "N"
      kinship = paste0("/proj/steinlab/projects/R00/eQTLanalysis/genofiles/snpsdifwo",k,".hBN.kinf")
      raw.cov = read.table("/proj/steinlab/projects/R00/eQTLanalysis/genofiles/covariates/new.pc.neu12.txt")
  }else{
      L = "F"
      kinship = paste0("/proj/steinlab/projects/R00/eQTLanalysis/genofiles/snpsfetwo",k,".hBN.kinf")
      raw.cov = read.table("/proj/steinlab/projects/R00/eQTLanalysis/genofiles/covariates/pc.fet.txt")
  }
  if (type == "progenitor" | type == "neuron"){
     pheno = paste0("/proj/steinlab/projects/R00/eQTLanalysis/phenofiles/vstNorm/",L,"-chr",k,"-",data$gene[j],".txt")
     geno = paste0("/proj/steinlab/projects/R00/eQTLanalysis/genofiles/Truncatedgenotypes/",type,"/newchr",k,"/",L,"-chr",k,"-",data$gene[j])
  }else{
     pheno = paste0("/proj/steinlab/projects/R00/eQTLanalysis/phenofiles/fetal/",L,"-chr",k,"-",data$gene[j],".txt")
     geno = paste0("/proj/steinlab/projects/R00/eQTLanalysis/genofiles/Truncatedgenotypes/fetal/nchr",k,"/",L,"-chr",k,"-",data$gene[j])
  }


  all.dosage = makeRaw(snp=data$SNP_B[j],chr=k,out=paste0(link1,data$gene[j],"_",data$snp[j],"_",data$SNP_B[j]))
  new.cov = data.frame(raw.cov,all.dosage)
  write.table(new.cov,file=paste0(link1,data$gene[j],"_",data$snp[j],"_",data$SNP_B[j],"_",gwas,".txt"),quote=FALSE,col.names=FALSE,row.names=FALSE)
  cov = paste0(link1,data$gene[j],"_",data$snp[j],"_",data$SNP_B[j],"_",gwas,".txt")
  cond = emmax(snp=data$snp[j],geno,pheno,kinship,cov,pval,paste0(link1,L,"-chr",k,"-",data$gene[j],"_",data$snp[j],"_",data$SNP_B[j],"_",gwas))

  if (nrow(cond) >= 1 & cond$V3 > pvalue) {
     #r2 = getR2(data$snp[j],data$SNP_B[j],k,out=link1)
     res = rbind(res, data.frame(eQTLsnp=data$snp[j],inibeta = data$beta[j], pval = data$pval[j], gene=data$gene[j], GWASsnp=data$SNP_B[j],Condbeta=cond$V2,Condpval=cond$V3,r2 = data$r2[j],pop=data$POP[j]))

  }
}
# Assign gene symbols and biotypes here

library(ensembldb)
library(EnsDb.Hsapiens.v86)
names(listTables(EnsDb.Hsapiens.v86))
# Genes from Hg38 relase v86
edb = EnsDb.Hsapiens.v86
wholeRange = genes(edb)
range = data.frame(wholeRange)

res$symbol = numeric(nrow(res))
res$biotype = numeric(nrow(res))

#sub.range = range[which(range$gene_id %in% intersect(range$gene_id,res$gene)),]
#ind = which(res$gene %in% intersect(range$gene_id,res$gene))

#res$symbol[ind] = sub.range$gene_name
#res$biotype[ind] = sub.range$gene_biotype

res$symbol = range$gene_name[match(res$gene,range$gene_id)]
res$biotype = range$gene_biotype[match(res$gene,range$gene_id)]

write.table(res,file=paste0("/pine/scr/n/i/nil1/Coloc.",type,"_",gwas,"_chr",k,".txt"),quote=FALSE,col.names=TRUE,row.names=FALSE)

}



