#!/bin/bash
#SBATCH -p steinlab
#SBATCH -n 1
#SBATCH --mem=35g
#SBATCH -t 3-00:00:00


module add star/2.6.0a
module add samtools

genomeDir="/proj/steinlab/projects/R00/r00-mrna-seq/data/reference_genome/STAR/GRCh38_release_92_pAAVhSYNeGFP/STARindex_GRCh38_release_92_pAAVhSYNeGFP"
datadir="/pine/scr/n/i/nil1/WASP/find_intersecting_snps/"
outdir="/pine/scr/n/i/nil1/WASP/remap/"

rna=$1;

star --runThreadN 8 \
     --genomeDir ${genomeDir} \
     --readFilesCommand gunzip -c --outFileNamePrefix ${outdir}/${rna} \
     --readFilesIn ${datadir}/${rna}.fq1.gz ${datadir}/${rna}.fq2.gz \
     --outSAMtype BAM Unsorted \

